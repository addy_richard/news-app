<?php
/**
 * Created by PhpStorm.
 * User: BalLack
 * Date: 12/21/2018
 * Time: 9:23 PM
 */

function redirect($page){
    header("location:".URL_ROOT.'/'.$page);
}

function createDate($datetime){
    $date = new DateTime($datetime);

    $date = $date->format('d-m-Y');

    return $date;
}

function getCommentsCount($id){
    $db = new Database();
    $sql = 'SELECT * FROM comments WHERE news_id = :id';

    $db->query($sql);
    $db->bind(':id', $id);
    $db->execute();

    $count = $db->rowCount();

    if ($count > 0){
        return $count;
    }else{
        return 0;
    }
}

function getTotalCatNews($cat_id){
    $db = new Database();
    $sql = 'SELECT * FROM news WHERE cat_id = :cat_id';

    $db->query($sql);
    $db->bind(':cat_id',$cat_id);
    $db->execute();
    $count = $db->rowCount();

    return $count;
}

function readMore($string){
    $content = explode(" " , $string);
    $words  = array_slice($content , 0 , 30);
    return implode(" " , $words);
}
function timeAgo($time){
    $time_ago = strtotime($time);
    $current_time = time();
    $time_difference = $current_time - $time_ago;

    $seconds = $time_difference;
    $minutes = round($seconds / 60);
    $hours = round($seconds / 3600);
    $days = round($seconds / 86400);
    $weeks = round($seconds / 604800);
    $months = round($seconds / 2629440);
    $years = round($seconds / 31553280);

    if ($seconds <= 60){
        return "Just now";
    }
    elseif ($minutes <= 60){
        if ($minutes == 1){
            return "a minute ago";
        }else{
            return "$minutes minutes ago";
        }
    }
    elseif ($hours <=24){
        if ($hours == 1){
            return "hour ago";
        }else{
            return "$hours hours ago";
        }
    }
    elseif ($days <= 7){
        if($days == 7){
            return "yesterday";
        }else{
            return "$days days ago";
        }
    }
    elseif ($weeks <= 4.3){

        if ($weeks == 1){
            return "a week ago";
        }else{
            return "$weeks weeks ago";
        }
    }
    elseif ($months <= 12){

        if ($months == 1){
            return "1 month ago";
        }else{
            return "$months months ago";
        }
    }
    else{
        if ($years == 1){
            return "1 year ago";
        }else{
            return "$years years ago";
        }
    }
}