<?php
/**
 * Created by PhpStorm.
 * User: BalLack
 * Date: 12/21/2018
 * Time: 9:08 PM
 */

require_once 'config/config.php';
require_once 'helpers/url_helper.php';

spl_autoload_register(function ($className){
    require_once 'libraries/'.$className. '.php';
});