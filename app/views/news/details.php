<?php require APP_ROOT.'/views/inc/header.php'; ?>
<?php include APP_ROOT . '/views/inc/navbar.php'; ?>

<div class="container">

    <div class="row">
        <div class="col-md-8 my-5">


                <div class="card mb-5">
                    <img class="card-img-top news-img" src="<?php echo URL_ROOT; ?>/public/imgs/<?php echo $data['news']->img; ?>"
                         alt="News 1 images">
                    <div class="card-body">
                        <h3 class="card-title">
                            <?php echo $data['news']->title; ?>
                        </h3>
                        <p class="card-text">
                            <?php echo $data['news']->content; ?>
                        </p>
                        <p class="card-text">
                            <small class="text-muted mr-5">posted: <?php echo timeAgo($data['news']->created_at); ?></small>
                            <small>
                                <a class="card-link text-dark">
                                    <i class="fa fa-comment-alt mr-2">
                                        <small class="badge">
                                            <?php echo $data['count'] ?>
                                        </small>
                                    </i>
                                </a>
                                <a class="card-link text-dark">
                                    <i class="fa fa-eye"><small class="badge">8</small></i>

                                </a>
                            </small>

                        </p>
                    </div>
                </div>


            <section id="comment-section">

                <!-- Comments Form -->
                <div class="card my-5">
                    <h5 class="card-header">Leave a Comment:</h5>
                    <div class="card-body">
                        <form method="post" action="<?php echo URL_ROOT; ?>/news/details">
                            <div class="form-group">
                                <textarea id="comment" name="comment" required class="form-control" rows="3"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>

                <!-- Single Comment -->
                <?php if ($data['comments']) : ?>
                    <?php foreach ($data['comments'] as $comment) : ?>

                        <div class="media mb-4">
                            <img class="d-flex mr-3 rounded-circle avatar" src="<?php echo URL_ROOT; ?>/public/imgs/user.png" alt="avatar">
                            <div class="media-body">
                                <div class="card">
                                    <div class="card-header">
                                        Commenter Name  <span class="text-muted comment-time">
                                        <i class="fa fa-circle"></i>
                                            <?php echo timeAgo($comment->comment_date); ?>
                                    </span>

                                    </div>
                                    <div class="card-body">
                                        <p class="card-text">
                                            <?php echo $comment->comment; ?>
                                        </p>
                                    </div>
                                    <div class="card-footer text-right border-0">
                                        <a href="#" class="">
                                            <i class="fa fa-reply"> reply</i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php endforeach; ?>
                <?php endif; ?>

            </section>

        </div>

        <div class="col-md-4 my-5">
            <?php require_once APP_ROOT . '/views/inc/sidebar.php'; ?>
        </div>
    </div>

</div>


<?php include APP_ROOT . '/views/inc/footer.php'; ?>
