
<?php require APP_ROOT.'/views/inc/header.php'; ?>
<?php include APP_ROOT . '/views/inc/navbar.php'; ?>

<div class="container">

    <div class="row">
        <div class="col-md-8 my-5">

            <?php foreach ($data['healthLifestyleNews'] as $news) : ?>

                <div class="card mb-5">
                    <img class="card-img-top news-img" src="<?php echo URL_ROOT?>/public/imgs/<?php echo $news->img; ?>"
                         alt="News 1 images">
                    <div class="card-body">
                        <h3 class="card-title">
                            <a href="<?php echo URL_ROOT; ?>/news/details?id=<?php echo $news->id; ?>" class="card-link"><?php echo $news->title; ?></a>
                        </h3>
                        <p class="card-text">
                            <?php echo readMore($news->content) . "..."; ?>
                            <a href="<?php echo URL_ROOT; ?>/news/details?id=<?php echo $news->id; ?>" class="btn btn-info btn-sm">Read more</a>
                        </p>
                        <p class="card-text">
                            <small class="text-muted">posted: <?php echo timeAgo($news->created_at); ?></small>
                            <small class="float-lg-right">
                                <a href="<?php echo URL_ROOT; ?>/news/details?id=<?php echo $news->id; ?>" class="card-link text-dark">
                                    <i class="fa fa-comment-alt mr-2">
                                        <small class="badge">
                                            <?php echo getCommentsCount($news->id); ?>
                                        </small>
                                    </i>
                                </a>
                                <a class="card-link text-dark">
                                    <i class="fa fa-eye"><small class="badge">8</small></i>

                                </a>
                            </small>
                        </p>
                    </div>
                </div>

            <?php endforeach; ?>

        </div>

        <div class="col-md-4 my-5">
            <?php require_once APP_ROOT . '/views/inc/sidebar.php'; ?>
        </div>
    </div>

</div>


<?php include APP_ROOT . '/views/inc/footer.php'; ?>
