
<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">

    <div class="container">
        <a href="<?php echo URL_ROOT; ?>" class="navbar-brand">
            <img src="<?php echo URL_ROOT; ?>/public/imgs/todo-icon.png" alt="logo" width="45px">
            <?php echo SITE_NAME; ?>
        </a>

        <button class="navbar-toggler" data-toggle="collapse" data-target="#menu">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="menu">

            <ul class="navbar-nav ml-auto">

                <li class="nav-item mx-2">
                    <a href="<?php echo URL_ROOT; ?>" class="nav-link <?php echo $data['page']; ?>">Home</a>
                </li>

                <?php foreach ($data['categories'] as $category) :  ?>

                    <?php
                    $link = $category->category;
                    $link = preg_replace("/[^a-zA-Z]+/","",$link);

                    $link = lcfirst($link);
                    ?>

                    <li class="nav-item mx-2">
                        <a href="<?php echo URL_ROOT; ?>/news/<?php echo $link; ?>"
                           class="nav-link <?php if ($data['page'] == $link) { echo 'active'; }else ''; ?>">

                            <?php echo $category->category; ?>
                        </a>
                    </li>

                <?php endforeach; ?>

            </ul>

        </div>
    </div>

</nav>