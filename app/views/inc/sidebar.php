<div class="card mb-5">
    <h5 class="card-header">Search</h5>
    <div class="card-body">
        <form action="<?php echo URL_ROOT; ?>/news/search" method="post">
            <div class="input-group">

                <input type="text" name="search" required class="form-control" placeholder="Search for...">
                <span class="input-group-btn input-group-prepend">
                      <input class="btn btn-secondary input-group-text" name="submit" type="submit" value="Go">
                    </span>

            </div>
        </form>
    </div>
</div>

<div class="card">
    <h5 class="card-header">Categories</h5>
    <div class="card-body">

        <ul class="navbar-nav ml-auto">


            <?php foreach ($data['categories'] as $category) :  ?>

                <?php
                $link = $category->category;
                $link = preg_replace("/[^a-zA-Z]+/","",$link);

                $link = lcfirst($link);
                ?>

                 <li class="nav-item mx-2">
                     <a class="nav-link" href="<?php echo URL_ROOT; ?>/news/<?php echo $link; ?>">
                       <?php echo $category->category; ?>
                         <span class="badge float-right text-dark">
                            <?php echo getTotalCatNews($category->id); ?>
                        </span>
                     </a>
                  </li>



            <?php endforeach; ?>

        </ul>

    </div>
</div>