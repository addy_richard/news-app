<?php require_once APP_ROOT . '/views/inc/header.php'; ?>

    <div class="container">
        <div class="row">
            <div class="col-md-5 mx-auto">

                <div class="card-body bg-light login-card">
                    <h2>Login</h2>
                    <p>Enter your email and password to login</p>
                    <hr>
                    <form action="" method="post" class="mt-5">

                        <div class="form-group">
                            <label for="email">Email: <sup>*</sup></label>
                            <input type="email" required value="<?php echo $data['email']; ?>"
                                   class="form-control form-control-lg <?php echo (!empty($data['email_err'])) ? 'is-invalid' : ''; ?>"
                                   placeholder="Email Address" name="email" id="email">
                            <span class="invalid-feedback"><?php echo $data['email_err']; ?></span>
                        </div>

                        <div class="form-group">
                            <label for="password">Password: <sup>*</sup></label>
                            <input type="password" value="<?php echo $data['password']; ?>"
                                   placeholder="Enter Password" required name="password" id="password"
                                   class="form-control form-control-lg <?php echo (!empty($data['password_err'])) ? 'is-invalid' : ''; ?>">

                            <span class="invalid-feedback"><?php echo $data['password_err']; ?></span>
                        </div>

                        <div class="row mt-5">
                            <div class="col">
                                <input type="submit" value="Login" class="btn btn-primary btn-block">
                            </div>
                            <div class="col">
                                <a href="<?php echo URL_ROOT; ?>/users/register" class="btn btn-block btn-light">Don't have account? Register</a>
                            </div>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>



<?php require_once APP_ROOT . '/views/inc/footer.php'; ?>
