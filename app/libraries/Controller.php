<?php
/**
 * Created by PhpStorm.
 * User: BalLack
 * Date: 12/21/2018
 * Time: 8:07 PM
 */
class Controller{
    public function model($modelFile){
        require_once '../app/models/'.$modelFile. '.php';

        return new $modelFile;
    }

    public function view($viewFile,$data = []){
        if (file_exists('../app/views/'.$viewFile.'.php')){

            require_once '../app/views/'.$viewFile . '.php';

        }else{
            redirect('news/index');
        }
    }
}