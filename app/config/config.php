<?php
/**
 * Created by PhpStorm.
 * User: BalLack
 * Date: 11/6/2018
 * Time: 2:13 PM
 */

//DB PARAMS
define('DB_HOST','localhost');
define('DB_USER','root');
define('DB_PASSWORD','');
define('DB_NAME','news_app');


// APP ROOT
define('APP_ROOT', dirname(dirname(__FILE__)));


// URL ROOT
define('URL_ROOT','http://localhost/newsApp');

// SITE NAME
define('SITE_NAME','News Today');

//APP VERSION
define('APP_VERSION','1.0.0');