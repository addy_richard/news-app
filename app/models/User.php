<?php
/**
 * Created by PhpStorm.
 * User: BalLack
 * Date: 12/22/2018
 * Time: 12:22 PM
 */

class User
{
    private $db;
    private $user;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function findUserByEmail($email){
        $sql = 'SELECT * FROM users WHERE email = :email';
        $this->db->query($sql);
        $this->db->bind(':email',$email);
        $this->user = $this->db->singleRow();

        if ($this->db->rowCount() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function loginUser($data){
        if ($this->findUserByEmail($data['email'])){
            $db_password = $this->user->password;

            if (password_verify($data['password'] , $db_password)){
                return $this->user;
            }else{
                return false;
            }
        }
    }

    public function registerUser($data){

    }

}