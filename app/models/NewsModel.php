<?php
/**
 * Created by PhpStorm.
 * User: BalLack
 * Date: 12/21/2018
 * Time: 11:12 PM
 */

class NewsModel
{
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function getCategories(){
        $sql = 'SELECT * FROM categories';

        $this->db->query($sql);

        $categories_result = $this->db->allRows();

        return $categories_result;
    }

    public function getAllNews(){
        $sql = 'SELECT *,
                news.id as id ,
                categories.id as catId
                FROM news
                INNER JOIN categories
                ON news.cat_id = categories.id
                ORDER BY news.created_at DESC ';

        $this->db->query($sql);

        $result = $this->db->allRows();

        return $result;
    }

    public function getCategoryNews($id){
        $sql = 'SELECT * FROM news WHERE cat_id = :cat_id ORDER BY news.created_at DESC';

        $this->db->query($sql);

        $this->db->bind(':cat_id',$id);
        $result = $this->db->allRows();

        return $result;
    }

    public function getSingleNews($id){
        $sql = 'SELECT * FROM news WHERE id = :id';

        $this->db->query($sql);
        $this->db->bind(':id', $id);
        $news = $this->db->singleRow();


        if ($this->db->rowCount() > 0){
            return $news;
        }else{
            return false;
        }
    }

    public function getNewsComments($newsId){
        $sql = 'SELECT * FROM comments WHERE news_id = :id ORDER BY comment_date DESC';

        $this->db->query($sql);
        $this->db->bind(':id',$newsId);
        $comments = $this->db->allRows();

        if ($this->db->rowCount() > 0){

            return $comments;
        }else{
            return false;
        }

    }

    public function getCommentsCount($newsId){
        $sql = 'SELECT * FROM comments WHERE news_id = :id';

        $this->db->query($sql);
        $this->db->bind(':id',$newsId);
        $this->db->execute();

        if ($this->db->rowCount() > 0){
            $count = $this->db->rowCount();

            return $count;
        }else{
            return false;
        }

    }

    public function postComment($data){
        $sql = 'INSERT INTO comments (news_id , user_id , comment) VALUES (:newsId , :userId , :comment)';

        $this->db->query($sql);
        $this->db->bind(':newsId' , $data['newsId']);
        $this->db->bind(':userId' , $data['userId']);
        $this->db->bind(':comment',$data['comment']);
        $this->db->execute();
    }

}