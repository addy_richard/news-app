<?php
/**
 * Created by PhpStorm.
 * User: BalLack
 * Date: 12/22/2018
 * Time: 12:22 PM
 */

class Users extends Controller
{

    public function __construct()
    {
        $this->userModel = $this->model('User');
    }

    public function login(){

        if ($_SERVER['REQUEST_METHOD'] == 'POST'){
            $_POST = filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);

            $data = [
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'email_err' => '',
                'password_err' => ''
            ];

            if (empty($data['email'])){
                $data['email_err'] = 'Please enter your email address';
            }else{
                if (!$this->userModel->findUserByEmail($data['email'])){
                    $data['email_err'] = 'No User found';
                }
            }

            if (empty($data['password'])){
                $data['password_err'] = 'Please enter your password';
            }

            if (empty($data['email_err']) && empty($data['password_err'])){
                $loggedinUser = $this->userModel->loginUser($data);

                if ($loggedinUser){

                }else{
                    $data['password_err'] = 'Password is incorrect';
                    $this->view('users/login',$data);
                }
            }else{
                $this->view('users/login',$data);
            }

        }else{
            $data = [
                'email' => '',
                'password' => '',
                'email_err' => '',
                'password_err' => ''
            ];
            $this->view('users/login',$data);
        }

    }

    public function register(){
        if ($_SERVER['REQUEST_METHOD'] == 'POST'){

            $_POST = filter_input_array(INPUT_POST , FILTER_SANITIZE_STRING);

            $data = [
              'firstname' => trim($_POST['first_name']),
              'lastname' => trim($_POST['lastname']),
              'email' => trim($_POST['email']),
              'password' => trim($_POST['password']),
              'confirm_password' => trim($_POST['confirm_password']),
              'firstname_err' => '',
              'lastname_err' => '',
              'email_err' => '',
              'password_err' => '',
              'confirm_password_err' => ''
            ];

            if (empty($data['firstname'])){
                $data['firstname_err'] = 'Please your first name';
            }

            if (empty($data['lastname'])){
                $data['lastname_err'] = 'Please enter your last name';
            }

            if (empty($data['email'])){
                $data['email_err'] = 'Please your email address';
            }else{

            }

            if (empty($data['password'])){
                $data['password_err'] = 'Please choose a password';
            }else{
                if (strlen($data['password']) < 6){
                    $data['password_err'] = 'Password must be at least 6 characters';
                }
            }

            if (empty($data['confirm_password'])){
                $data['confirm_password_err'] = 'Please confirm your password';
            }else{
                if ($data['confirm_password'] != $data['password']){
                    $data['confirm_password_err'] = 'Passwords do not match';
                }
            }

            if(empty($data['firstname_err']) && empty($data['lastname_err'])
                && empty($data['email_err']) && empty($data['password_err']) && empty($data['confirm_password_err'])){
                die('SUCCESS!!! All systems go!');
            }else{
                $this->view('users/register',$data);
            }
        }else{

            //INIT FORM DATA
            $data = [
                'firstname' => '',
                'lastname' => '',
                'email' => '',
                'password' => '',
                'confirm_password' => '',
                'firstname_err' => '',
                'lastname_err' => '',
                'email_err' => '',
                'password_err' => '',
                'confirm_password_err' => ''
            ];

            //LOAD VIEW
            $this->view('users/register',$data);

        }
    }

    public function index(){
        $data = [];

        $this->view('users/index',$data);
    }

}