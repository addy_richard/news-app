<?php
/**
 * Created by PhpStorm.
 * User: BalLack
 * Date: 12/21/2018
 * Time: 9:19 PM
 */

class News extends Controller
{
    public function __construct()
    {
        $this->newsModel = $this->model('NewsModel');
    }

    public function index(){
        $categories = $this->newsModel->getCategories();
        $news = $this->newsModel->getAllNews();


        $data = [
            'categories' => $categories,
            'news' => $news,
            'page' => 'active'
        ];
        $this->view('news/index',$data);
    }

    public function politics(){
        $categories = $this->newsModel->getCategories();
        $politicsNews = $this->newsModel->getCategoryNews(1);

        $data = [
            'categories' => $categories,
            'politicsNews' => $politicsNews,
            'page' => 'politics'

        ];

        $this->view('news/politics',$data);
    }

    public function healthLifestyle(){
        $categories = $this->newsModel->getCategories();
        $healthLifestyleNews = $this->newsModel->getCategoryNews(2);

        $data = [
            'categories' => $categories,
            'healthLifestyleNews' => $healthLifestyleNews,
            'page' => 'healthLifestyle'

        ];

        $this->view('news/healthLifestyle',$data);
    }

    public function scienceTech(){
        $categories = $this->newsModel->getCategories();
        $scienceTechNews = $this->newsModel->getCategoryNews(3);

        $data = [
            'categories' => $categories,
            'scienceTechNews' => $scienceTechNews,
            'page' => 'scienceTech'

        ];
       $this->view('news/scienceTech',$data);
    }



    public function sports(){
        $categories = $this->newsModel->getCategories();
        $sportsNews = $this->newsModel->getCategoryNews(4);

        $data = [
            'categories' => $categories,
            'sportsNews' => $sportsNews,
            'page' => 'sports'

        ];

        $this->view('news/sports',$data);
    }

    public function entertainment(){
        $categories = $this->newsModel->getCategories();
        $entertainmentNews = $this->newsModel->getCategoryNews(5);

        $data = [
            'categories' => $categories,
            'entertainmentNews' => $entertainmentNews,
            'page' => 'entertainment'

        ];

        $this->view('news/entertainment',$data);
    }

    public function search(){

        if ($_SERVER['REQUEST_METHOD'] == 'POST'){
            $search_str = filter_var(trim($_POST['search']),FILTER_SANITIZE_STRING);

            if (!empty($search_str)){
                $search_keys = '';
                $search_str_arr = explode(" " , $search_str);

                if (count($search_str_arr) > 0){
                    foreach ($search_str_arr as $key){
                     $search_keys .= "'%".$key."%' OR ";
                    }

                    $search_str = substr($search_keys,0,-4);

                    echo $search_str;
                }
            }else{
                redirect('news');
            }
        }
    }


    public function details(){
        session_start();
        if (isset($_GET['id'])){
            $newsId = (int)trim($_GET['id']);
            $_SESSION['newsId'] = $newsId;

            $newsDetails = $this->newsModel->getSingleNews($newsId);
            $categories = $this->newsModel->getCategories();
            $comments= $this->newsModel->getNewsComments($newsId);
            $commentsCount= $this->newsModel->getCommentsCount($newsId);
            if ($newsDetails){

                if ($commentsCount){

                    $data = [
                        'categories' => $categories,
                        'news' => $newsDetails,
                        'comments' => $comments,
                        'count' => $commentsCount
                    ];

                    $this->view('news/details',$data);

                }else{
                    $data = [
                        'categories' => $categories,
                        'news' => $newsDetails,
                        'comments' => $comments,
                        'count' => 0
                    ];

                    $this->view('news/details',$data);
                }

            }else{
                redirect('news');
            }

        }elseif ($_SERVER['REQUEST_METHOD'] == 'POST'){

            $comment = trim($_POST['comment']);


            if (!empty($comment)){

                $data = [
                    'newsId' => $_SESSION['newsId'],
                    'userId' => 2,
                    'comment' => $comment
                ];

                $this->newsModel->postComment($data);
                redirect('news/details?id='.$_SESSION['newsId']);
            }else{
                redirect('news/details?id='.$_SESSION['newsId']);
            }

        }else{
            redirect('news');
        }


    }
}

/*<span class="mt-0 comment-username d-block">

                                <span class="comment"><?php echo $comment->comment; */?><!--</span>-->